package optfn.com.appepp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Usuario on 23/10/2016.
 */

public class ReportTxt extends Activity {

    String asunto,mensaje,mail,nombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporttxt);
        loadPref();

    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
    private void loadPref(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        asunto = sp.getString("casunto","Solicitar Información");
        mensaje = sp.getString("ccontenido","Me pongo en contacto con usted para pedirle información.");
        mail = sp.getString("cdireccion","edi.pozo.pozo@gmail.com");
        nombre = sp.getString("cnombre","Def Name");
        EditText txtAsunto = (EditText) findViewById(R.id.txtAsunto);
        txtAsunto.setText(asunto);
        EditText txtMsg = (EditText) findViewById(R.id.txtSend);
        txtMsg.setText(mensaje);
    }
    public void enviar(View view){
        EditText txtMsg = (EditText) findViewById(R.id.txtSend);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        intent.putExtra(Intent.EXTRA_TEXT, txtMsg.getText());
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {mail});
        startActivity(intent);
        finish();
    }
}
