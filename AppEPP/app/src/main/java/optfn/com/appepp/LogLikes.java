package optfn.com.appepp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by Usuario on 03/12/2016.
 */

public class LogLikes extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView txtAbout = (TextView)findViewById(R.id.txtAbout);
        //txtAbout.setText(getString(R.string.aboutview)+" Edilberto");

        try {
            String nombre_fichero = getExternalFilesDir(null) + "/log_likes.txt";
            InputStream inputStream =  new FileInputStream(nombre_fichero);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String rdo = bufferedReader.readLine();
            txtAbout.setText(rdo);
            inputStream.close();
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
}


