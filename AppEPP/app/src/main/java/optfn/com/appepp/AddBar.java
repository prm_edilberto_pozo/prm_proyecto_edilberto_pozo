package optfn.com.appepp;

/**
 * Created by b13-06m on 01/02/2017.
 */

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static optfn.com.appepp.R.string.cod;

public class AddBar extends AppCompatActivity {


    static final int REQUEST_TAKE_PHOTO = 10;
    static final int REQUEST_POS = 111;

    private String photoPath;
    private String photoPath2;
    private String lat;
    private String lon;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);
        mImageView = (ImageView) findViewById(R.id.imageView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_TAKE_PHOTO){
            galleryAddPic();
            setPic();
        }
        if(requestCode == REQUEST_POS){
            lat = data.getStringExtra("lat");
            lon = data.getStringExtra("lon");
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "BAR_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        photoPath = /*"file:"+*/image.getAbsolutePath();
        photoPath2 = /*"file:"+*/image.getName();
        //photoPath = image.getAbsolutePath();
        return image;
    }
    public void cargar(View view){
        Intent i = new Intent(this, MapsActivity.class);

        i.putExtra("bar", "New");
        startActivityForResult(i,REQUEST_POS);

    }
    public void insertar(View view) {

        EditText etcb = (EditText)findViewById(R.id.txtCB);
        EditText etcn = (EditText)findViewById(R.id.txtN);
        EditText etcd = (EditText)findViewById(R.id.txtD);

        int codb = 0;
        if (photoPath2 == null){
            Toast.makeText(this,"Haz una foto",Toast.LENGTH_SHORT).show();
            return;
        }
        if (lat == null || lon == null){
            Toast.makeText(this,"Coge la ubicación",Toast.LENGTH_SHORT).show();
            return;
        }
        try {

            codb = Integer.parseInt(etcb.getText().toString());
        }catch (NumberFormatException e){
            Toast.makeText(this,"Inserta números",Toast.LENGTH_SHORT).show();
            return;
        }

        String nom = etcn.getText().toString();
        String des = etcd.getText().toString();

        bbddTapeo conexion = new bbddTapeo(AddBar.this, "bbddTapeo", null, 1);
        SQLiteDatabase bbdd = conexion.getWritableDatabase();
        Cursor c1 = bbdd.rawQuery("SELECT count(nombre) FROM Bares where cod = "+codb, null);
        if(c1.moveToFirst()){
            if(c1.getInt(0) == 1){
                conexion.close();
                bbdd.close();
                Toast.makeText(this,"El bar ya existe",Toast.LENGTH_SHORT).show();
                return;
            }
        }
        bbdd.execSQL("INSERT INTO Bares (cod, nombre, descripcion, imagen,lat,long) " +
                "VALUES (" + codb + ", '" + nom + "','" + des + "', '" + photoPath2 + "', '"+lat+"','"+lon+"')");
        conexion.close();
        bbdd.close();
        Toast.makeText(this,"Exito",Toast.LENGTH_SHORT).show();


        setResult(RESULT_OK);
        finish();
    }
    public void guardar(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                // Uri photoURI = FileProvider.getUriForFile(this,"optfn.com.capturafotos",photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));

                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }

}
