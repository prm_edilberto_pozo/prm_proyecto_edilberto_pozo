package optfn.com.appepp;


import android.Manifest;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CalendarContract;

import java.util.Calendar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

import static android.R.attr.width;
import static java.security.AccessController.getContext;
import static optfn.com.appepp.AddBar.REQUEST_POS;
import static optfn.com.appepp.R.id.fragmentb;
import static optfn.com.appepp.R.string.asunto;

public class MainActivity extends AppCompatActivity implements Comunicador {

    /**
     * APP DE TAPEO CIUDAD REAL CREADA POR @EdilbertoPozo
     * DAM-2 2016
     */

    private boolean dosFragmentos;

    //Progress Dialog
    private ProgressDialog pDialog;

    // url to get all products list
    private static String url = "http://www.edilbertopozo.esy.es/accesoservicio/select.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TAPAS = "Tapas";
    private static final String TAG_COD = "cod";
    private static final String TAG_COD_BAR = "cod_bar";
    private static final String TAG_NOMBRE = "nombre";
    private static final String TAG_IMG = "imagen";
    private static final String TAG_DESC = "descripcion";

    // products JSONArray
    JSONArray products = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragmentb) != null) {
            dosFragmentos = true;
            ImageButton b1 = (ImageButton) findViewById(R.id.BtnImagen);
            Button b2 = (Button) findViewById(R.id.btnAnterior);
            Button b3 = (Button) findViewById(R.id.btnSiguiente);
            Button b4 = (Button) findViewById(R.id.btnBorrar);
            b1.setVisibility(View.INVISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);
            b4.setVisibility(View.INVISIBLE);


        } else {
            this.setTitle("Bares");
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void responder(Bar bar) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentB fragmentb = (FragmentB) fragmentManager.findFragmentById(R.id.fragmentb);

        //if(fragmentb != null) {
        if (dosFragmentos) {
            Log.d("edi", "Entra land");
            fragmentb.cambiarTexto(bar.getDescripcion());
            fragmentb.cambiarImagen(bar.getImg());
            this.setTitle("Bar");
            //cambiar tamaño imagen
            ImageView iv_img = (ImageView) findViewById(R.id.iv_fragB);
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(300, 300);
            iv_img.setLayoutParams(parms);
            //Oculto botones tapa
            ImageButton b1 = (ImageButton) findViewById(R.id.BtnImagen);
            Button b2 = (Button) findViewById(R.id.btnAnterior);
            Button b3 = (Button) findViewById(R.id.btnSiguiente);
            Button b4 = (Button) findViewById(R.id.btnBorrar);
            b1.setVisibility(View.INVISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);
            b4.setVisibility(View.INVISIBLE);


        } else {


            Intent i = new Intent(this, ActivityB.class);

            i.putExtra("descripcion", bar.getDescripcion());
            String s = bar.getImg();
            if(s.equals("1")||s.equals("2")||s.equals("3")||s.equals("4")){
                i.putExtra("tipo", 1);
                i.putExtra("imagen", Integer.parseInt(bar.getImg()));
            }

            else{
                i.putExtra("tipo", 2);
                i.putExtra("imagen", bar.getImg());
            }
            i.putExtra("title", "Bar");
            startActivity(i);

        }
    }

    @Override
    public void ir(Bar bar) {

            Intent i = new Intent(this, MapsActivity.class);

            i.putExtra("lat", bar.getLat());
            i.putExtra("long", bar.getLong());
            i.putExtra("bar", bar.getNombre());
            startActivity(i);

    }

    @Override
    public void abrir(Bar bar) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentB fragmentb = (FragmentB) fragmentManager.findFragmentById(R.id.fragmentb);

        //if(fragmentb != null) {
        if (dosFragmentos) {
            Log.d("edi", "Entra land");
            this.setTitle("Tapa");
            //cambiar tamaño imagen
            ImageView iv_img = (ImageView) findViewById(R.id.iv_fragB);
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(500, 500);
            iv_img.setLayoutParams(parms);
            //Muestro botones tapa
            ImageButton b1 = (ImageButton) findViewById(R.id.BtnImagen);
            Button b2 = (Button) findViewById(R.id.btnAnterior);
            Button b3 = (Button) findViewById(R.id.btnSiguiente);
            Button b4 = (Button) findViewById(R.id.btnBorrar);
            b1.setVisibility(View.VISIBLE);
            b2.setVisibility(View.VISIBLE);
            b3.setVisibility(View.VISIBLE);
            b4.setVisibility(View.VISIBLE);

            fragmentb.mostrarTapas(bar.getCod());

        } else {


            Intent i = new Intent(this, ActivityB.class);

            i.putExtra("descripcion", bar.getDescripcion());
            i.putExtra("imagen", bar.getImg());
            i.putExtra("title", "Tapa");
            i.putExtra("cod_bar", bar.getCod());
            startActivity(i);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater;// = getMenuInflater();
        if (Build.VERSION.SDK_INT > 15)
            inflater = getMenuInflater();
        else
            inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    public void reportb(MenuItem item) {
        Intent intent = new Intent(this, ReportTxt.class);
        startActivity(intent);
    }

    public void right(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://es.creativecommons.org/blog/licencias/"));
        startActivity(intent);
    }

    public void phone(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_CALL,
                Uri.parse("tel:693714239"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                         int[] grantResults);
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    public void about(MenuItem item) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    public void loglikes(MenuItem item) {
        Intent intent = new Intent(this, LogLikes.class);
        startActivity(intent);
    }

    public void borrarlog(MenuItem item) {
        try {
            String nombre_fichero = getExternalFilesDir(null) + "/log_likes.txt";
            FileOutputStream fOS = new FileOutputStream(nombre_fichero, false);

            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                fOS.write("->".getBytes());
            }
            fOS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logborrado(MenuItem item) {
        Intent intent = new Intent(this, LogBorrado.class);
        startActivity(intent);
    }

    public void calen(MenuItem item) {


// Construct event details
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2017, 9, 14, 15, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(2017, 9, 17, 22, 45);
        endMillis = endTime.getTimeInMillis();

// Insert Event
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        TimeZone timeZone = TimeZone.getDefault();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        values.put(CalendarContract.Events.TITLE, "Tapeo");
        values.put(CalendarContract.Events.DESCRIPTION, "Ruta del tapeo CIUDAD REAL!");
        values.put(CalendarContract.Events.CALENDAR_ID, 3);
        Uri uri = cr.insert(Uri.parse("content://com.android.calendar/events"), values);

       
    }

    public void borrarlogborrado(MenuItem item) {
        try {
            String nombre_fichero = "log_borrado.txt";
            FileOutputStream fOS = openFileOutput(nombre_fichero, Context.MODE_PRIVATE);
            fOS.write("->".getBytes());
            fOS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void restaurar(MenuItem item) {
        bbddTapeo conexion = new bbddTapeo(this, "bbddTapeo", null, 1);
        SQLiteDatabase bbdd = conexion.getWritableDatabase();
        bbdd.execSQL("UPDATE Tapas SET bLike=0 WHERE bLike=2");
        bbdd.execSQL("UPDATE Tapas SET bLike=1 WHERE bLike=3");
        conexion.close();
        bbdd.close();
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public void cargartapas(MenuItem item) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String fecha = sp.getString("cversion", "20161212");
        new LeerProducto().execute(fecha);
    }

    public void preferences(MenuItem item) {

        Intent intent = new Intent(this, setPreferenceActivity.class);
        startActivityForResult(intent, 0);
    }

    public void addTapa(MenuItem item) {

        Intent intent = new Intent(this, AddTapa.class);
        startActivity(intent);
    }
    public void addTapa2(MenuItem item) {

        Intent intent = new Intent(this, AddTapa2.class);
        startActivity(intent);
    }
    public void addBar(MenuItem item) {

        Intent intent = new Intent(this, AddBar.class);
        startActivityForResult(intent,777);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 777){
            Intent intent=new Intent();
            intent.setClass(this, this.getClass());
            //llamamos a la actividad
            this.startActivity(intent);
            //finalizamos la actividad actual
            this.finish();
        }

    }
    /*public void cargartapas(MenuItem item){

        Intent intent = new Intent(this, setPreferenceActivity.class);
        startActivityForResult(intent, 0);

    }*/

    public void pulsar(View view) {
        Intent i = new Intent(this, ActivityB.class);
        startActivity(i);
    }



    class LeerProducto extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Cargando las tapas. Por favor, espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            JSonParser jParser = new JSonParser();

            //obtener el JSON from URL
            JSONObject json = jParser.getJSONFromUrl(url, args[0]);


            try {
                //Verificamos si la select devolvió datos
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // Ha devuelto datos
                    // Obtenemos el array que tiene los productos que en nuestro caso sólo tiene un producto
                    products = json.getJSONArray(TAG_TAPAS);

                    // Bucle para recorrer todos los productos
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String nombre = c.getString(TAG_NOMBRE);
                        String cod = c.getString(TAG_COD);
                        String cod_bar = c.getString(TAG_COD_BAR);
                        String desc = c.getString(TAG_DESC);
                        String img = c.getString(TAG_IMG);

                        publishProgress(nombre, cod, cod_bar, desc, img);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onProgressUpdate(String... args) {
            bbddTapeo conexion = new bbddTapeo(MainActivity.this, "bbddTapeo", null, 1);
            SQLiteDatabase bbdd = conexion.getWritableDatabase();
            bbdd.execSQL("INSERT INTO Tapas (cod, cod_bar, nombre, bLike, descripcion, imagen) " +
                    "VALUES (" + args[1] + ", " + args[2] + ", '" + args[0] + "', 0, '" + args[3] + "', '" + args[4] + "')");
            conexion.close();
            bbdd.close();

        }


        protected void onPostExecute(String file_url) {

            pDialog.dismiss();
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage(getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            SharedPreferences.Editor editor = sp.edit();
            Date date = new Date();
            String m = (String) DateFormat.format("MM", date);
            String y = (String) DateFormat.format("yyyy", date);
            String d = (String) DateFormat.format("dd", date);
            editor.putString("cversion", y + m + d);
            editor.commit();
            startActivity(i);
        }

    }


}
