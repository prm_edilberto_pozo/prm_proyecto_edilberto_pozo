package optfn.com.appepp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Usuario on 03/12/2016.
 */
public class bbddTapeo extends SQLiteOpenHelper {

    public bbddTapeo(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version) {

        super(contexto, nombre, factory, version);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        //Creamos las tablas necesarias cuando la bbdd es nueva
        db.execSQL("CREATE TABLE Tapas (cod INTEGER, cod_bar INTEGER, nombre TEXT, bLike INTEGER, descripcion TEXT, imagen TEXT)");
        db.execSQL("CREATE TABLE Bares (cod INTEGER, nombre TEXT, descripcion TEXT, imagen TEXT, lat TEXT, long TEXT)");
        //Metemos taps y bares
        db.execSQL("INSERT INTO Tapas (cod, cod_bar, nombre, bLike, descripcion, imagen) " +
                "VALUES (111, 1111, 'Tapa1', 0, 'Descripción tapa 1', 'img1'), " +
                "(112, 1111, 'Tapa 2', 0, 'Descripción tapa 2', 'img2'), " +
                "(113, 1111, '3a Tapa', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(114, 1111, 'Tapa 4a', 0, 'Descripción tapa 1', 'img4'), " +
                "(221, 2222, 'Tapa1-2', 0, 'Descripción tapa 1', 'img1'), " +
                "(222, 2222, 'Tapa 2-2', 0, 'Descripción tapa 2', 'img2'), " +
                "(223, 2222, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(224, 2222, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4'), " +
                "(331, 3333, 'Tapa1-2', 0, 'Descripción tapa 1', 'img1'), " +
                "(332, 3333, 'Tapa 2-2', 0, 'Descripción tapa 2', 'img2'), " +
                "(333, 3333, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(334, 3333, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4'), " +
                "(443, 4444, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(444, 4444, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4')," +
                "(445, 4444, 'Tapa foto', 0, 'Tapa foto', 'TAPA_1.jpg')");

        db.execSQL("INSERT INTO Bares (cod, nombre, descripcion, imagen,lat,long) " +
                "VALUES (1111, 'BAR 1', 'Descripción Bar 1', 'img1','38.992656', '-3.926800'), " +
                "(2222, '2 BAR', 'El bar 2 nanananana', 'img2','38.986018', '-3.929117'), " +
                "(3333, '3er BAR 1', 'Descripción Bar 3', 'img3','38.956018', '-3.969117'), " +
                "(4444, '4 BAR 4', 'Descripción Bar 4', 'img4','38.962018', '-3.944117')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {

        //Eliminamos las tablas

        db.execSQL("DROP TABLE IF EXISTS Tapas");
        db.execSQL("DROP TABLE IF EXISTS Bares");

        //Volvemos a crear las tablas

        db.execSQL("CREATE TABLE Tapas (cod INTEGER, cod_bar INTEGER, nombre TEXT, bLike INTEGER, descripcion TEXT, imagen TEXT)");
        db.execSQL("CREATE TABLE Bares (cod INTEGER, nombre TEXT, descripcion TEXT, imagen TEXT)");
        //Metemos taps y bares
        db.execSQL("INSERT INTO Tapas (cod, cod_bar, nombre, bLike, descripcion, imagen) " +
                "VALUES (111, 1111, 'Tapa1', 0, 'Descripción tapa 1', 'img1'), " +
                "(112, 1111, 'Tapa 2', 0, 'Descripción tapa 2', 'img2'), " +
                "(113, 1111, '3a Tapa', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(114, 1111, 'Tapa 4a', 0, 'Descripción tapa 1', 'img4'), " +
                "(221, 2222, 'Tapa1-2', 0, 'Descripción tapa 1', 'img1'), " +
                "(222, 2222, 'Tapa 2-2', 0, 'Descripción tapa 2', 'img2'), " +
                "(223, 2222, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(224, 2222, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4'), " +
                "(331, 3333, 'Tapa1-2', 0, 'Descripción tapa 1', 'img1'), " +
                "(332, 3333, 'Tapa 2-2', 0, 'Descripción tapa 2', 'img2'), " +
                "(333, 3333, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(334, 3333, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4'), " +
                "(443, 4444, '3a Tapa-2', 0, 'La tapa 3 nanananana', 'img3'), " +
                "(444, 4444, 'Tapa 4a-22', 0, 'Descripción tapa 1', 'img4')," +
                "(445, 4444, 'Tapa foto', 0, 'Tapa foto', 'TAPA_1.jpg')");

        db.execSQL("INSERT INTO Bares (cod, nombre, descripcion, imagen) " +
                "VALUES (1111, 'BAR 1', 'Descripción Bar 1', 'img1'), " +
                "(2222, '2 BAR', 'El bar 2 nanananana', 'img2'), " +
                "(3333, '3er BAR 1', 'Descripción Bar 3', 'img3'), " +
                "(4444, '4 BAR 4', 'Descripción Bar 4', 'img4')");

    }

}