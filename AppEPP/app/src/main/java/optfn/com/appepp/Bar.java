package optfn.com.appepp;

/**
 * Created by Usuario on 19/11/2016.
 */

public class Bar {
    private int cod;
    private String nombre;
    private String descripcion;
    private String img;
    private double lat;
    private double lon;
    public Bar(int cod, String nombre,String descripcion,String img,String lat, String lon){
        this.cod = cod;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.img = img;
        this.lat = Double.parseDouble(lat);
        this.lon = Double.parseDouble(lon);

    }

    public int getCod() {
        return cod;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getImg() {
        return img;
    }
    public double getLat() {
        return lat;
    }
    public double getLong() {
        return lon;
    }
}
