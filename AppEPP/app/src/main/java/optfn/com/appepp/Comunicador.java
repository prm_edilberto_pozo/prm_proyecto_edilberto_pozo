package optfn.com.appepp;

/**
 * Created by b13-06m on 17/11/2016.
 */

public interface Comunicador {
    public void responder(Bar bar);
    public void abrir(Bar bar);
    public void ir(Bar bar);
}
