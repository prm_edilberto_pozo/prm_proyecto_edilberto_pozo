package optfn.com.appepp;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Usuario on 11/11/2016.
 */

public class Preferencia extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);


    }
}
