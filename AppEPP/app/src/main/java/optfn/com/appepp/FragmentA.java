package optfn.com.appepp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static android.os.Build.VERSION_CODES.M;
import static android.os.FileObserver.DELETE;
import static optfn.com.appepp.R.id.iv_img;

/**
 * Created by b13-06m on 14/11/2016.
 */

public class FragmentA extends Fragment implements AdapterView.OnItemClickListener {

    private ListView list;
    Comunicador comunicacion;
    int ClickedItemIndex;
    @Override

    public void onActivityCreated (Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);

        comunicacion = (Comunicador) getActivity();
        registerForContextMenu(list);
        list.setOnItemClickListener(this);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmentaelement, container, false);
        //now you must initialize your list view
        list =(ListView)rootView.findViewById(R.id.list);
        //ACCESO A TODOS LOS REGISTROS BBDD
        bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
        SQLiteDatabase bbdd = conexion.getReadableDatabase();
//        Cursor c1 = bbdd.rawQuery("SELECT cod,nombre,descripcion,imagen,lat,long FROM Bares where cod in (select distinct cod_bar from Tapas where bLike in (0,1))", null);
        Cursor c1 = bbdd.rawQuery("SELECT cod,nombre,descripcion,imagen,lat,long FROM Bares", null);
        LinkedList<Bar> lista = new LinkedList<Bar>();
        if (c1.moveToFirst()) {
            do {
                int img;
                switch (c1.getString(3)) {
                    case "img1":
                        img = 1;
                        break;
                    case "img2":
                        img = 2;
                        break;
                    case "img3":
                        img = 3;
                        break;
                    case "img4":
                        img = 4;
                        break;
                    default:
                        img = 1;
                        break;
                }
                lista.add(new Bar(c1.getInt(0),c1.getString(1),c1.getString(2),c1.getString(3),c1.getString(4),c1.getString(5)));
            } while (c1.moveToNext());
        }
        c1.close();
        bbdd.close();
        conexion.close();
        list.setAdapter(new Adaptador(getActivity(),lista));
        list.setOnItemClickListener(this);

        return rootView;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        //saber posicion
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo)menuInfo;
        ClickedItemIndex = info.position;
        //poner titulo
        menu.setHeaderTitle(((Bar)list.getAdapter().getItem(info.position)).getNombre());
        //inflar menu
        inflater.inflate(R.menu.menu_context, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mcInfo:
                comunicacion.abrir((Bar)list.getAdapter().getItem(ClickedItemIndex));
                break;
            case R.id.mCBar:
                comunicacion.responder((Bar)list.getAdapter().getItem(ClickedItemIndex));
                break;
            case  R.id.mcLocalizacion:
                comunicacion.ir((Bar)list.getAdapter().getItem(ClickedItemIndex));
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        comunicacion.abrir((Bar)list.getAdapter().getItem(position));
    }


}
