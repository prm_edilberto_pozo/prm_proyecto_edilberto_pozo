package optfn.com.appepp;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by practicasiap on 11/12/2016.
 */


public class JSonParserInsert {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    private static final String TAG_FECHA= "fecha";
    private static final String TAG_COD = "cod";
    private static final String TAG_COD_BAR = "cod_bar";
    private static final String TAG_NOMBRE = "nombre";
    private static final String TAG_IMG = "imagen";
    private static final String TAG_DESC = "descripcion";

    // constructor
    public JSonParserInsert() {

    }

    public  JSONObject getJSONFromUrl(String urlInput, String cod, String cod_bar, String nombre, String descripcion, String imagen, String fecha) {

        HttpURLConnection connection = null;
        BufferedReader readerRespuesta = null;
        BufferedWriter writerLlamada = null;
        OutputStream streamLlamada = null;
        InputStream streamRespuesta = null;
        StringBuffer bufferRespuesta = null;


        try{
            URL url = new URL(urlInput);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.connect();

            //Establecemos los parámetros
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(TAG_COD, cod)
                    .appendQueryParameter(TAG_COD_BAR, cod_bar)
                    .appendQueryParameter(TAG_NOMBRE, nombre)
                    .appendQueryParameter(TAG_DESC, descripcion)
                    .appendQueryParameter(TAG_IMG, imagen)
                    .appendQueryParameter(TAG_FECHA, fecha);
            String parametros = builder.build().getEncodedQuery();

            //Creamos un stream de salida y escribimos en él los parámetros POST
            streamLlamada = connection.getOutputStream();
            writerLlamada = new BufferedWriter(new OutputStreamWriter(streamLlamada));
            writerLlamada.write(parametros);
            writerLlamada.flush();

            //Obtenemos la respuesta
            streamRespuesta = connection.getInputStream();
            readerRespuesta = new BufferedReader(new InputStreamReader(streamRespuesta));

            //Leemos los valores devueltos y los añadimos a un StringBuffer
            bufferRespuesta = new StringBuffer();
            String line ="";
            while ((line = readerRespuesta.readLine()) != null){
                    bufferRespuesta.append(line);
            }

            //Convertimos a String el buffer de String
            //creamos un JSON a partir de la respuesta
            json = bufferRespuesta.toString();
            jObj = new JSONObject(json);

            //Cerramos todo
            readerRespuesta.close();
            streamRespuesta.close();
            writerLlamada.close();
            streamLlamada.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Devolvemos el JSON
        return jObj;
    }
}
