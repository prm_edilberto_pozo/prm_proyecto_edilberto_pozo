package optfn.com.appepp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by b13-06m on 14/11/2016.
 */

public class ActivityB extends AppCompatActivity {

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_b);
        this.setTitle(this.getIntent().getStringExtra("title"));
        if(this.getIntent().getStringExtra("title").equals("Bar")){
            //Oculto botones tapa
            ImageButton b1 = (ImageButton) findViewById(R.id.BtnImagen);
            Button b2 = (Button) findViewById(R.id.btnAnterior);
            Button b3 = (Button) findViewById(R.id.btnSiguiente);
            b1.setVisibility(View.INVISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);
            FragmentB fb = (FragmentB)getSupportFragmentManager().findFragmentById(R.id.fragmentb);
            fb.cambiarTexto(this.getIntent().getStringExtra("descripcion"));
            if(this.getIntent().getIntExtra("tipo",1) == 1)
                fb.cambiarImagen(this.getIntent().getIntExtra("imagen",1));
            else
                fb.cambiarImagen(this.getIntent().getStringExtra("imagen"));
        }else{
            //Muestro botones tapa
            ImageButton b1 = (ImageButton) findViewById(R.id.BtnImagen);
            Button b2 = (Button) findViewById(R.id.btnAnterior);
            Button b3 = (Button) findViewById(R.id.btnSiguiente);
            b1.setVisibility(View.VISIBLE);
            b2.setVisibility(View.VISIBLE);
            b3.setVisibility(View.VISIBLE);
            FragmentB fb = (FragmentB)getSupportFragmentManager().findFragmentById(R.id.fragmentb);
            //Mostrar tapas del bar del intent
            fb.mostrarTapas(this.getIntent().getIntExtra("cod_bar",4444));
        }
        //cambiar tamaño imagen
        ImageView iv_img = (ImageView) findViewById(R.id.iv_fragB);
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(700, 700);
        iv_img.setLayoutParams(parms);

    }

}