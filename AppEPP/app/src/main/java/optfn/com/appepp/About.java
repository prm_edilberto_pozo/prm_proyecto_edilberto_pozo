package optfn.com.appepp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by b13-06m on 20/10/2016.
 */

public class About extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView txtAbout = (TextView)findViewById(R.id.txtAbout);
        //txtAbout.setText(getString(R.string.aboutview)+" Edilberto");

        try {
            InputStream inputStream = this.getResources().openRawResource(R.raw.about);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String rdo = bufferedReader.readLine();
            txtAbout.setText(rdo);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
}
