package optfn.com.appepp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

import static android.R.attr.factor;
import static optfn.com.appepp.R.id.iv_img;

/**
 * Created by Usuario on 16/11/2016.
 */

public class Adaptador extends BaseAdapter {

    private Activity actividad;
    private LinkedList<Bar> lista;

    public Adaptador(Activity actividad, LinkedList<Bar> lista){

        super();

        this.actividad = actividad;
        this.lista = lista;



    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = actividad.getLayoutInflater();

        View view = inflater.inflate(R.layout.list_items, null, true);
        ImageView mImageView = (ImageView)view.findViewById(iv_img);
        switch (lista.get(position).getImg()){
            case "img1":
                mImageView.setImageResource(R.drawable.img_1);
                break;
            case "img2":
                mImageView.setImageResource(R.drawable.img_2);
                break;
            case "img3":
                mImageView.setImageResource(R.drawable.img_3);
                break;
            case "img4":
                mImageView.setImageResource(R.drawable.img_4);
                break;
            default:

                String photoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+lista.get(position).getImg();
//                int targetW = mImageView.getWidth();
//                int targetH = mImageView.getHeight();

                 //Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(photoPath, bmOptions);
//                int photoW = bmOptions.outWidth;
//                int photoH = bmOptions.outHeight;

                // Determine how much to scale down the image
//                int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
//                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inSampleSize = 8;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
                //Bitmap bitmap = BitmapFactory.decodeFile(photoPath);
                mImageView.setImageBitmap(bitmap);
                break;
        }
        TextView tv_name = (TextView)view.findViewById(R.id.tv_name);
        tv_name.setText(lista.get(position).getNombre());
        TextView tv_description = (TextView)view.findViewById(R.id.tv_description);
        tv_description.setText(lista.get(position).getDescripcion());

        return view;
    }
}