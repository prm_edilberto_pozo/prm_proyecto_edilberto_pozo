package optfn.com.appepp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Usuario on 13/12/2016.
 */

public class AddTapa extends AppCompatActivity {

    //Progress Dialog
    private ProgressDialog pDialog;

    // url to get all products list
    private static String url = "http://www.edilbertopozo.esy.es/accesoservicio/insert.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";


    EditText lblCod;
    EditText lblCodBar;
    EditText lblImagen;
    EditText lblPass;
    EditText lblNombre;
    EditText lblDesc;
    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        lblCod = (EditText) findViewById(R.id.txtCod);
        lblCodBar = (EditText) findViewById(R.id.txtCodBar);
        lblImagen = (EditText) findViewById(R.id.txtImagen);
        lblPass = (EditText) findViewById(R.id.txtPass);
        lblNombre = (EditText) findViewById(R.id.txtNombre);
        lblDesc = (EditText) findViewById(R.id.txtDesc);
    }

    public void insertar (View view) {
        if(lblPass.getText().toString().equals("nanana")){
            new InsertTapa().execute(lblCod.getText().toString(),
                    lblCodBar.getText().toString(),
                    lblNombre.getText().toString(),
                    lblDesc.getText().toString(),
                    lblImagen.getText().toString());
        }else lblCod.setText("Pass no valida");


    }

    class InsertTapa extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddTapa.this);
            pDialog.setMessage("Guardando. Por favor, espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            JSonParserInsert jParser = new JSonParserInsert();

            // ponemos la fecha
            Date date = new Date();
            String m = (String) android.text.format.DateFormat.format("MM", date);
            String y = (String) android.text.format.DateFormat.format("yyyy", date);
            String d = (String) android.text.format.DateFormat.format("dd", date);
            String fecha =  y+m+d;
            //obtener el JSON from URL
            JSONObject json = jParser.getJSONFromUrl(url, args[0],args[1],args[2],args[3],args[4],fecha);


            try {
                //Verificamos si la select devolvió datos
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // Ha devuelto datos

                        publishProgress("Correcto");

                } else  publishProgress(json.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }



        protected void onProgressUpdate(String... args){

            lblCod.setText(args[0]);
        }


        protected void onPostExecute(String file_url) {

            pDialog.dismiss();
        }

    }
}
