package optfn.com.appepp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Usuario on 03/12/2016.
 */

public class LogBorrado extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView txtAbout = (TextView)findViewById(R.id.txtAbout);
        //txtAbout.setText(getString(R.string.aboutview)+" Edilberto");

        try {
            String nombre_fichero = "log_borrado.txt";
            InputStream inputStream = openFileInput(nombre_fichero);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String rdo = bufferedReader.readLine();
            txtAbout.setText(rdo);
            inputStream.close();
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
}


