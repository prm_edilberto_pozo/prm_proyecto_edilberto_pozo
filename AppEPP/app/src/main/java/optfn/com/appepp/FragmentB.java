package optfn.com.appepp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;

import static android.R.attr.button;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static optfn.com.appepp.R.id.iv_img;
import static optfn.com.appepp.R.id.txtAbout;

/**
 * Created by b13-06m on 14/11/2016.
 */

public class FragmentB extends Fragment {

    private Cursor cur;
    private int codTapa;
    private int codBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentbelement,
                container, false);
        Button bAnt = (Button) view.findViewById(R.id.btnAnterior);
        bAnt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //DATO A DATO BBDD
                ImageButton bLike = (ImageButton) getActivity().findViewById(R.id.BtnImagen);
                if (cur.moveToPrevious()) {
                    switch (cur.getString(5)) {
                        case "img1":
                            cambiarImagen(1);
                            break;
                        case "img2":
                            cambiarImagen(2);
                            break;
                        case "img3":
                            cambiarImagen(3);
                            break;
                        case "img4":
                            cambiarImagen(4);
                            break;
                        default:
                            cambiarImagen(cur.getString(5));
                            break;
                    }
                    cambiarTexto(cur.getString(2)+"\n"+cur.getString(4));
                    codTapa = cur.getInt(0);
                    if(cur.getInt(3) == 0){
                        bLike.setImageResource(R.drawable.ic_nolike);
                    }else bLike.setImageResource(R.drawable.ic_like);


                }
            }
        });
        ImageButton bLike = (ImageButton) view.findViewById(R.id.BtnImagen);
        bLike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                //MODIFICADO BBDD
                int aux = cur.getPosition();
                if(cur.getInt(3)==0){
                    bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
                    SQLiteDatabase bbdd = conexion.getWritableDatabase();
                    bbdd.execSQL("UPDATE Tapas SET bLike=1 WHERE cod= "+String.valueOf(codTapa));
                    conexion.close();
                    bbdd.close();
                    doLog("LIKE -- "+cur.getString(2)+" ");
                    update(aux);

                }else{
                    bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
                    SQLiteDatabase bbdd = conexion.getWritableDatabase();
                    bbdd.execSQL("UPDATE Tapas SET bLike=0 WHERE cod= "+String.valueOf(codTapa));
                    conexion.close();
                    bbdd.close();
                    doLog("Dislike -- "+cur.getString(2)+" "); // escribir log
                    update(aux);
                }



            }
        });
        Button bBorrar = (Button) view.findViewById(R.id.btnBorrar);
        bBorrar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Realizo un borrado lógico ya que no quiero borrar las tapas de la bbdd
                if(cur.getInt(3)==0){
                    bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
                    SQLiteDatabase bbdd = conexion.getWritableDatabase();
                    bbdd.execSQL("UPDATE Tapas SET bLike=2 WHERE cod= "+String.valueOf(codTapa));
                    conexion.close();
                    bbdd.close();
                    doLogBorrado("-- DEL "+cur.getString(2)+" ");
                    Intent i = getActivity().getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);


                }else{
                    bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
                    SQLiteDatabase bbdd = conexion.getWritableDatabase();
                    bbdd.execSQL("UPDATE Tapas SET bLike=3 WHERE cod= "+String.valueOf(codTapa));
                    conexion.close();
                    bbdd.close();
                    doLogBorrado("-- DEL "+cur.getString(2)+" "); // escribir log
                    Intent i = getActivity().getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            }
        });
        Button bSig = (Button) view.findViewById(R.id.btnSiguiente);
        bSig.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //DATO A DATO BBDD
                ImageButton bLike = (ImageButton) getActivity().findViewById(R.id.BtnImagen);
                if (cur.moveToNext()) {
                    switch (cur.getString(5)) {
                        case "img1":
                            cambiarImagen(1);
                            break;
                        case "img2":
                            cambiarImagen(2);
                            break;
                        case "img3":
                            cambiarImagen(3);
                            break;
                        case "img4":
                            cambiarImagen(4);
                            break;
                        default:
                            cambiarImagen(cur.getString(5));
                            break;
                    }
                    cambiarTexto(cur.getString(2)+"\n"+cur.getString(4));
                    codTapa = cur.getInt(0);
                    if(cur.getInt(3) == 0){
                        bLike.setImageResource(R.drawable.ic_nolike);
                    }else bLike.setImageResource(R.drawable.ic_like);


                }
            }
        });
        return view;


    }

    //ESCRITURA MEM EXTERNA
    private void doLog(String s) {
        final String sl = System.getProperty("line.separator");
        try {
            String nombre_fichero = getContext().getExternalFilesDir(null) + "/log_likes.txt";
            FileOutputStream fOS = new FileOutputStream(nombre_fichero, true);
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                fOS.write(s.getBytes());
            }
            fOS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //ESCRITURA MEM INTERNA
    private void doLogBorrado(String s) {

        try {
            String nombre_fichero = "log_borrado.txt";
            FileOutputStream fOS = getActivity().openFileOutput(nombre_fichero, Context.MODE_APPEND);
            fOS.write(s.getBytes());
            fOS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void cambiarTexto(String texto){

        TextView tv= (TextView) getActivity().findViewById(R.id.tv_FragB);
        if(tv != null)
            tv.setText(texto);

    }
    public void cambiarImagen(int pos){

        ImageView iv_img = (ImageView) getActivity().findViewById(R.id.iv_fragB);
        switch (pos){
            case 1:
                iv_img.setImageResource(R.drawable.img_1);
                break;
            case 2:
                iv_img.setImageResource(R.drawable.img_2);
                break;
            case 3:
                iv_img.setImageResource(R.drawable.img_3);
                break;
            case 4:
                iv_img.setImageResource(R.drawable.img_4);
                break;
            default:
                iv_img.setImageResource(R.drawable.img_1);
                break;
        }

    }
    public void cambiarImagen(String loc){

        ImageView mImageView = (ImageView) getActivity().findViewById(R.id.iv_fragB);
        String photoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+loc;
//        int targetW = mImageView.getWidth();
//        int targetH = mImageView.getHeight();
//
//        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
//        int photoW = bmOptions.outWidth;
//        int photoH = bmOptions.outHeight;
//
//        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = 4;
//        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
//        Bitmap bitmap = BitmapFactory.decodeFile(photoPath);
        mImageView.setImageBitmap(bitmap);




    }

    public void update(int pos_cur){

        //DATO ESPECIFICO BBDD
        bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
        SQLiteDatabase bbdd = conexion.getReadableDatabase();
        cur = bbdd.rawQuery("SELECT cod,cod_bar,nombre,bLike,descripcion,imagen FROM Tapas where bLike in (0,1) and cod_bar = "+String.valueOf(codBar), null);
        ImageButton bLike = (ImageButton) getActivity().findViewById(R.id.BtnImagen);
        //DATO ESPECIFICO BBDD
        if (cur.moveToPosition(pos_cur)) {
            cambiarTexto(cur.getString(2)+"\n"+cur.getString(4));
            if(cur.getInt(3) == 0){
                bLike.setImageResource(R.drawable.ic_nolike);
            }else bLike.setImageResource(R.drawable.ic_like);
        }

    }
    public void mostrarTapas(int cod_bar){
        codBar = cod_bar;
        //TODOS LO DATOS BBDD
        bbddTapeo conexion = new bbddTapeo (getActivity(), "bbddTapeo", null, 1);
        SQLiteDatabase bbdd = conexion.getReadableDatabase();
        cur = bbdd.rawQuery("SELECT cod,cod_bar,nombre,bLike,descripcion,imagen FROM Tapas where bLike in (0,1) and cod_bar = "+String.valueOf(cod_bar), null);
        ImageButton bLike = (ImageButton) getActivity().findViewById(R.id.BtnImagen);
        if (cur.moveToFirst()) {
                switch (cur.getString(5)) {
                    case "img1":
                        cambiarImagen(1);
                        break;
                    case "img2":
                        cambiarImagen(2);
                        break;
                    case "img3":
                        cambiarImagen(3);
                        break;
                    case "img4":
                        cambiarImagen(4);
                        break;
                    default:
                        cambiarImagen(cur.getString(5));
                        break;
                }
            cambiarTexto(cur.getString(2)+"\n"+cur.getString(4));
            codTapa = cur.getInt(0);
            if(cur.getInt(3) == 0){
                bLike.setImageResource(R.drawable.ic_nolike);
            }else bLike.setImageResource(R.drawable.ic_like);


        }

    }
}
