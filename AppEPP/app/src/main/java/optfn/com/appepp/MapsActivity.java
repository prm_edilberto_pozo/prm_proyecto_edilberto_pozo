package optfn.com.appepp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    CameraUpdate cam;
    private LatLng punto;
    private String bar;
    PolylineOptions linea;

    private LocationListener locListener;
    private LocationManager locManager;

    private int pedir_permiso_gps = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        bar = this.getIntent().getStringExtra("bar");
        double l = this.getIntent().getDoubleExtra("lat", 38.99);
        double ln = this.getIntent().getDoubleExtra("long", -3.92);
        punto = new LatLng(l,ln);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(!bar.equals("New")){
            mMap.addMarker(new MarkerOptions().position(punto).title(bar));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(punto));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(punto, 15));

            linea= new PolylineOptions();



            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                public void onMapLongClick(LatLng point) {
                    double lat=point.latitude;
                    double lng=point.longitude;
                    String tituloMarcador="Mi marcador "+ String.valueOf(lat)+", "+ String.valueOf(lng);
                    mMap.addMarker(
                            new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                                    .title(tituloMarcador));
                    //puntoant=new LatLng(punto.latitude, punto.longitude);
                    //punto=new LatLng(point.latitude, point.longitude);



                    linea.add(new LatLng(point.latitude, point.longitude));
                    linea.width(3);
                    linea.color(Color.GREEN);
                    mMap.addPolyline(linea);



                }
            });
        }else{

            mMap.moveCamera(CameraUpdateFactory.newLatLng(punto));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(punto, 15));

            linea= new PolylineOptions();

            final Intent i = new Intent(this, AddBar.class);

            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                public void onMapLongClick(LatLng point) {
                    double lat=point.latitude;
                    double lon=point.longitude;
                    String.valueOf(lat);
                    i.putExtra("lat", String.valueOf(lat));
                    i.putExtra("lon", String.valueOf(lon));

                    setResult(RESULT_OK,i);
                    finish();
                }
            });
        }


    }


    public void pamadrid(View view){
        int permissionCheck = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, pedir_permiso_gps);
        }
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            return;
        }


        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (loc != null) {

            LatLng lll = new LatLng(loc.getLatitude(),loc.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLng(lll));
            mMap.addMarker(new MarkerOptions().position(lll).title("Tu posición"));

        }

        locListener = new LocationListener() {

            public void onLocationChanged(Location location) {

                if (location != null) {

                    LatLng lll = new LatLng(location.getLatitude(),location.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(lll));

                }

            }

            public void onProviderDisabled(String provider) {

            }

            public void onProviderEnabled(String provider) {

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

        };

        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, locListener);

    }



    public void padentro(View view){
        //LatLng Madrid = new LatLng(40.417325, -3.683081);
        //CameraUpdateFactory.newLatLngZoom(Madrid, 5F);
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Madrid, 5F));
        cam= CameraUpdateFactory.zoomIn();

        mMap.animateCamera(cam);
    }

    public void pafuera(View view){
        //LatLng Madrid = new LatLng(40.417325, -3.683081);
        //CameraUpdateFactory.newLatLngZoom(Madrid, 5F);
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Madrid, 2F));
        cam= CameraUpdateFactory.zoomOut();

        mMap.animateCamera(cam);
    }



}
